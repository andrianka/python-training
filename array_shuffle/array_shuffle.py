def array_shuffle(array):
    """
    Return array with shuffle to the left side.
    Function get array.
    """

    tmp = array[0]
    for i in range(len(array)-1):
        array[i] = array[i+1]
    array[-1] = tmp
    return array
