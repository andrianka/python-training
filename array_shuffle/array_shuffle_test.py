import unittest

from array_shuffle import array_shuffle


class ArrayShuffleTest(unittest.TestCase):
    def test_shuffle_to_left(self):
        """
        Test that shuffle elments to left side
        """
        A = [1, 2, 3, 4, 5]

        self.assertEqual(array_shuffle(A), [2, 3, 4, 5, 1])


if __name__ == "__main__":
    unittest.main()
